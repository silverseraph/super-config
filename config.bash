#!/bin/bash

pkg_mgr=""
pkgs=""

unset -f has_exe

function has_exe() {
    \which "${1}" 2> /dev/null > /dev/null;
    if [ "$?" -ne 0 ]; then
        echo 0;
    else
        echo 1;
    fi
}

if [ $(has_exe dnf) -ne 0 ]; then
    echo "Found dnf"
    precmd=""
    pkg_mgr="$(\which dnf)";
    pkgs="gcc gcc-c++ vim-enhanced cmake cmake-fedora extra-cmake-modules python python3 python-devel python3-devel python-pip python3-pip luajit lua tmux clang clang-libs clang-devel"
fi

if [ $(has_exe greadlink) -ne 0 ]; then
    alias readlink='greadlink '
fi

echo "Using \"${pkg_mgr}\" to install \"${pkgs}\""

#install with system package manager
if [ "$pkg_mgr" != "" -a "$pkgs" != "" ]; then
    if [ ! -z "${precmd}" ]; then
        eval "sudo ${pkg_mgr} ${precmd}"
    fi
    eval "sudo ${pkg_mgr} install ${pkgs}"
else
    echo "No supported package manger found.  Will try to proceed without installation."
fi

#lua package installation
if [ $(has_exe luarocks) -ne 0 ]; then
    for i in luajson inifile luasocket; do
        #check if the package exists, and if it doesn't install it
        luarocks show "$i" 2> /dev/null > /dev/null
        if [ "$?" -ne 0 ]; then
            sudo luarocks install "$i"
        fi
    done
fi

function sym_install() {
    src=$(readlink -m $1)
    dst=$(readlink -m $2)

    #fail if the number of args is too few
    if [ "$#" -lt 2 ]; then
        echo "ERROR: sym_install $@"
        return
    fi

    if [ "${src}" == "${dst}" ]; then
        return
    fi

    mkdir -p "$(dirname ${dst})"

    #age old version if there is an existing file that isn't this one
    if [ -e "${dst}" -a "${src}" != "${dst}" ]; then
        scripts/age "${dst}"
    fi

    #default to standard executable
    if [ -z "$3" ]; then
        mode="$3"
    else
        mode="755"
    fi

    #change permissions as needed
    chmod "$mode" "${src}"

    #create symlink
    if [ -z "$DEBUG" -o "$DEBUG" == "false" ]; then
        ln -s "${src}" "${dst}"
    fi
}

#conditionally create the bin directory
if [ -e "~/bin" ]; then
    if [ ! -d "~/bin" ]; then
        echo "Cannot copy scripts.  \"~/bin\" already exists, and is not a directory. Exiting..."
        exit -1
    fi
else
    mkdir -p ~/bin
fi

#copy scripts
sym_install scripts/age ~/bin/age 755
sym_install scripts/ds ~/bin/ds 755
sym_install scripts/latexcount.pl ~/bin/latexcount.pl 755
sym_install scripts/slfind ~/bin/slfind 755
sym_install scripts/srfind ~/bin/srfind 755
sym_install scripts/swap ~/bin/swap 755
sym_install scripts/mquery ~/bin/mquery 755
sym_install scripts/ipq ~/bin/ipq 755

#install the configs
sym_install configs/.aliases ~/.aliases 644
sym_install configs/.bashrc ~/.bashrc 644
sym_install configs/.dhexrc ~/.dhexrc 644
sym_install configs/.gitconfig ~/.gitconfig 644
sym_install configs/.latexmkrc ~/.latexmkrc 644
sym_install configs/.screenrc ~/.screenrc 644
sym_install configs/.tmux.conf ~/.tmux.conf 644
sym_install configs/.vimrc ~/.vimrc 644
sym_install configs/starship.toml ~/.config/starship.toml

#install code libs
sym_install lualib ~/lua/lib 755

#try to configure tmux
if [ ! -d ~/.tmux/plugins/tpm ]; then
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi
