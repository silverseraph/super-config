## Super Config

Handles \*nix system configuration by copying useful and critical scripts into the proper places.  Personal information is removed from these scripts to prevent security issues.  The installer also includes a bunch of other config files for dhex, vim, git, bash, etc.  This should make getting used to new systems easier. 
