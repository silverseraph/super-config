# .bashrc

export LANG=en_US.utf8

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

function has_exe() {
    \which "${1}" 2> /dev/null > /dev/null;
    if [ "$?" -ne 0 ]; then
        echo 0
    else
        echo 1
    fi
}

if [[ $TERM =~ screen* ]]; then
    export TERM='screen-256color'
else
    export TERM='xterm-256color'
fi

export CLICOLOR=1
set -o noclobber
shopt -s dotglob

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/usr/lib:/usr/lib64"

export LIBFFI_LIBS="-L${HOME}/lib -L${HOME}/lib64 -lffi"
export LIBFFI_CFLAGS="-I${HOME}/include"
export PKG_CONFIG_PATH="${HOME}/share/pkgconfig:${HOME}/lib/pkgconfig:${HOME}/lib64/pkgconfig:/usr/lib64/pkgconfig:/usr/share/pkgconfig:/usr/lib/pkgconfig:/usr/local/share/aclocal:/usr/local/lib/pkgconfig"

if [ -f ~/.aliases ];
then
    source ~/.aliases
fi

if [ -f ~/.bash_extras ];
then
    source ~/.bash_extras
fi

red="$(tput setaf 1 2> /dev/null)"
blue="$(tput setaf 4 2> /dev/null)"
reset="$(tput sgr0 2> /dev/null)"

export PS1='\[$red\][\u@\h\[$reset\] \[$blue\]\W\[$reset\]\[$red\]]\$\[$reset\] '

export LD_LIBRARY_PATH="/usr/local/lib:/usr/local/lib64:${LD_LIBRARY_PATH}"

export COLORS="/etc/DIR_COLORS.256color"

#general config
export PATH="${HOME}/bin:${PATH}"

#lua configs
export LUA_VERSION="$(lua -v | cut '-d ' -f 2 | rev | cut -d. -f2- | rev)"
export LUA_PATH="/usr/share/lua/${LUA_VERSION}/?.lua;/usr/share/lua/${LUA_VERSION}/?/init.lua;/usr/local/share/lua/${LUA_VERSION}/?/init.lua;/usr/local/share/lua/${LUA_VERSION}/?.lua;${HOME}/.luarocks/share/lua/${LUA_VERSION}/?.lua;${HOME}/.luarocks/share/lua/${LUA_VERSION}/?/init.lua;;/usr/lib/lua/${LUA_VERSION}/?;/usr/lib/lua/${LUA_VERSION}/?.lua;/usr/lib64/lua/${LUA_VERSION}/?.lua;/usr/lib64/lua/${LUA_VERSION}/?/init.lua;./?.lua;${HOME}/lua/lib/all/?;${HOME}/lua/lib/all/?.lua;${HOME}/lua/lib/${LUA_VERSION}/?;${HOME}/lua/lib/${LUA_VERSION}/?.lua"
export LUA_CPATH="/usr/lib/lua/${LUA_VERSION}/?.so;${HOME}/.luarocks/lib/lua/${LUA_VERSION}/?.so;/usr/lib64/lua/${LUA_VERSION}/?.so;/usr/lib64/lua/${LUA_VERSION}/loadall.so;./?.so;${HOME}/lua/lib/all/?;${HOME}/lua/lib/all/?.so;${HOME}/lua/lib/${LUA_VERSION}/?;${HOME}/lua/lib/${LUA_VERSION}/?.so"

#golang config
export GOROOT="${HOME}/.go"
export GOPATH="${HOME}/go"

export PROMPT_COMMAND='printf "\033]0;%s\007" `basename "${PWD/${HOME}/\~}"`'

unset SSH_ASKPASS

# python versioning
PYTHON_VERSION="$(/usr/bin/env python --version 2>&1)"
export PYTHON_VERSION=$(echo "${PYTHON_VERSION}" | cut '-d ' -f2 | rev | cut -d. -f2- | rev)

if [ $(has_exe kubectl) ]; then
    source <(kubectl completion bash)
fi

export PATH="${GOPATH}/bin:${PATH}"

export GPG_TTY="$(tty)"

if [ $(has_exe stty) ]; then
    stty werase 
fi

if [ -f "${HOME}/.cargo/env" ]; then
    source "$HOME/.cargo/env"
fi

if [ $(has_exe starship) ]; then
    eval "$(starship init bash)"
fi

if [ -f "${HOME}/.asdf/asdf.sh" ]; then
    source "${HOME}/.asdf/asdf.sh"
fi

if [ -f "${HOME}/.asdf/completions/asdf.bash" ]; then
    source "${HOME}/.asdf/completions/asdf.bash"
fi
