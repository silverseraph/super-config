set encoding=utf-8
set scriptencoding utf-8
set nocompatible
set hidden

"general settings
set pastetoggle=<F2>
set guicursor=
set cursorline
set number
set t_Co=256
set t_ut=
set noerrorbells
set ruler
set lazyredraw
set nomodeline
set colorcolumn=80

"mouse workaround for tmux
set mouse+=a
if &term =~ '^screen' && !has('nvim')
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif

" insert mode
inoremap <C-h> <C-w>

"fkey bindings
nnoremap <space> za
nn       <F3>    :MundoToggle<CR>
nn       <F4>    :TagbarToggle<CR>
nn       <F5>    :TlistToggle<CR>
nn       <F8>    :NERDTreeToggle<CR>
nn       <F9>    :call ReviewLastCommit()<CR>

nnoremap ; :

"visual mode
vnoremap <Leader>s :sort<CR>
vmap <expr> > ShiftAndKeepVisualSelection(">")
vmap <expr> < ShiftAndKeepVisualSelection("<")

"normal mode
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <tab> :tabn<CR>
nnoremap <S-tab> :tabp<CR>
nnoremap <C-t> :tabnew<CR>

noremap <C-F> <C-W><C-F>

"terminal mode (neovim)
if has('nvim')
    tnoremap <Esc> <C-\><C-n>
    tnoremap <C-[> <C-\><C-n>
endif

function! Chtab(tabsize, ...)
    let &tabstop=str2nr(a:tabsize)
    let &shiftwidth=str2nr(a:tabsize)
    set list
    set listchars=tab:>\ |
    let l:usetab = a:0 > 0 ? a:1 : 0
    if l:usetab
        set noexpandtab
    else
        set expandtab
    endif
endfunction

set splitright
set splitbelow

set autoindent
set incsearch
set hlsearch
set ignorecase
set smartcase
set backspace=indent,eol,start
set complete-=i
set completeopt=longest,menuone
set smartindent
set expandtab
set autoread
set laststatus=2
call Chtab(4)

syntax on
filetype plugin indent on
set foldmethod=indent
set noswapfile

"special functions for plugins
function! BuildYCM(info) "taken from vim-plug's git page
    if a:info.status == 'installed' || a:info.force
        !./install.sh --clang-completer --system-libclang
    endif
endfunction

"plugins here
call plug#begin('~/.vim/plugged')

" color schemes
Plug 'https://github.com/andrewbenton/Revolution.vim.git'
Plug 'whatyouhide/vim-gotham'
Plug 'tomasr/molokai'
Plug 'flazz/vim-colorschemes'
Plug 'chriskempson/vim-tomorrow-theme'

"completion
Plug 'Valloric/YouCompleteMe'

Plug 'w0rp/ale'
Plug 'sjl/AnsiEsc.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'mattn/emmet-vim', { 'for': ['xml', 'html', 'php', 'htm', 'jsx', 'vue'] }
Plug 'cappyzawa/fly-lint.vim'
Plug 'gregsexton/gitv'
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-easymotion.vim'
Plug 'haya14busa/incsearch-fuzzy.vim'
Plug 'Yggdroot/LeaderF', { 'do': './install.sh' }
Plug 'tmhedberg/matchit'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tyru/open-browser.vim'
Plug 'weirongxu/plantuml-previewer.vim'
Plug 'aklt/plantuml-syntax'
Plug 'kannokanno/previm'
Plug 'kien/rainbow_parentheses.vim'
Plug 'rust-lang/rust.vim'
Plug 'vim-syntastic/syntastic'
Plug 'godlygeek/tabular'
Plug 'majutsushi/tagbar'
Plug 'vim-scripts/taglist.vim'
Plug 'wellle/targets.vim'
Plug 'leafgarland/typescript-vim'
Plug 'SirVer/ultisnips'
Plug 'Shougo/vimproc.vim', {'do': 'make'}
Plug 'bling/vim-airline'
Plug 'vim-autoformat/vim-autoformat'
Plug 'ntpeters/vim-better-whitespace'
Plug 'bufbuild/vim-buf'
Plug 'liuchengxu/vim-clap'
Plug 'kchmck/vim-coffee-script', {'for': ['coffee']}
Plug 'luan/vim-concourse'
Plug 'rhysd/vim-crystal'
Plug 'ap/vim-css-color'
Plug 'sirsireesh/vim-dlang-phobos-highlighter', {'for': ['d', 'dlang'] }
Plug 'idanarye/vim-dutyl', {'for': ['d', 'dlang'] }
Plug 'easymotion/vim-easymotion'
Plug 'alpaca-tc/vim-endwise'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tikhomirov/vim-glsl', {'for': ['glsl']}
Plug 'fatih/vim-go'
Plug 'towolf/vim-helm'
Plug 'digitaltoad/vim-jade', {'for': ['jade', 'diet', 'dt', 'pug'] }
Plug 'pangloss/vim-javascript'
Plug 'google/vim-jsonnet'
Plug 'maxmellon/vim-jsx-pretty',
Plug 'artur-shaik/vim-javacomplete2', { 'for': ['java', 'kotlin'] }
Plug 'plasticboy/vim-markdown'
Plug 'terryma/vim-multiple-cursors'
Plug 'simnalamburt/vim-mundo'
Plug 'mustache/vim-mustache-handlebars'
Plug 'tsandall/vim-rego'
Plug 'xavierchow/vim-sequence-diagram', {'for': ['seq', 'sequence', 'wsd'] }
Plug 'honza/vim-snippets'
Plug 'kana/vim-submode'
Plug 'tpope/vim-repeat'
Plug 'airblade/vim-rooter'
Plug 'timakro/vim-searchant'
Plug 'segeljakt/vim-silicon'
Plug 'tpope/vim-surround'
Plug 'cespare/vim-toml'
Plug 'tkztmk/vim-vala', { 'for': [ 'vala' ] }
Plug 'idanarye/vim-vebugger'
Plug 'posva/vim-vue'
Plug 'liuchengxu/vista.vim'
Plug 'mattn/webapi-vim'
Plug 'HerringtonDarkholme/yats.vim'

call plug#end()

"editorconfig settings
let s:editor_config_path = exepath('editorconfig')
if len(s:editor_config_path) > 0
    let g:EditorConfig_exec_path = s:editor_config_path
else
    let g:EditorConfig_exec_path = '/usr/local/bin/editorconfig'
endif
let g:EditorConfig_core_mode = 'external_command'

"nerdtree settings
let g:NERDTreeWinPos = 'right'
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 1
let g:NERTTreeDirArrows = 1

"ycm settings
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py"'
let g:ycm_confirm_extra_conf = 0
let g:ycm_autoclose_preview_window_after_completion = 1
command! Cp let g:ycm_autoclose_preview_window_after_completion = 0
command! Ch let g:ycm_autoclose_preview_window_after_completion = 1

"ale settings
let g:ale_echo_msg_error_str = 'ERROR'
let g:ale_echo_msg_warning_str = 'WARNING'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

"airline settings
"let g:airline#extensions#whitespace#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ycm#enabled = 1
let g:airline_left_sep = ''
let g:airline_right_sep = ''

"vim-gitgutter settings
let g:gitgutter_max_signs = 10000
highlight clear SignColumn
set updatetime=500
nmap [c <Plug>(GitGutterPrevHunk)
nmap ]c <Plug>(GitGutterNextHunk)
nmap <Leader>hs <Plug>(GitGutterStageHunk)
nmap <Leader>hu <Plug>(GitGutterUndoHunk)

"emmet-vim settings
imap hh <C-y>,

"flake8
let g:flake8_max_line_length = 200

"latex settings
let g:tex_flavor = 'latex'

"git settings
let @m = 'iSigned-off-by: Andrew Benton <Andrew.Benton675@gmail.com>'

"ctrlp settingsettings
nmap pp <C-p>
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_match_window = 'botton,order:btt,min:1,max:20,results:20'
let g:ctrlp_extensions = ['autoignore']
set wildignore+=*/tmp*,*.so,*.swp,*.zip

"leaderf

"matchit settings
if !exists('g:loaded_matchit') && findfile('plugin/matchit.vim', &runtimepath) ==# ''
    runtime! macros/matchit.vim
endif

"vim-sequence-diagram settings
nmap <unique> <leader>t <Plug>GenerateDiagram

"vim-daylight settings
set background=dark
"set background=light
"color Revolution
color gruvbox

"color gruvbox
let g:daylight_morning_color_term   = 'Revolution'
let g:daylight_afternoon_color_term = 'Revolution'
let g:daylight_evening_color_term   = 'gotham256'
let g:daylight_late_color_term      = 'gotham256'
let g:daylight_morning_color_gvim   = 'Revolution'
let g:daylight_afternoon_color_gvim = 'Revolution'
let g:daylight_evening_color_gvim   = 'gotham256'
let g:daylight_late_color_gvim      = 'gotham256'

let g:daylight_late_hour      = 20.1
let g:daylight_morning_hour   = 6
let g:daylight_afternoon_hour = 6.1
let g:daylight_evening_hour   = 20

"syntastic settings
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
"syntastic-vala
let g:syntastic_vala_modules = ['gee-1.0', 'lua']
"syntastic-java
let g:syntastic_java_checkers = ['checkstyle']
let g:syntastic_java_checkstyle_conf_file = './checkstyle.xml'

"previm settings
let g:previm_open_cmd = 'xdg-open'
let g:previm_enable_realtime = 1
let g:previm_show_header = 1

"neocomplcache
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_min_syntax_length = 2
let g:neocomplcache_enable_underbar_completion = 1
let g:neocomplcache_dictionary_filename_lists = { 'default' : '', 'vimshell' : $HOME.'/.vimshell_hist'}
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
" Enable omni completion.
augroup omnicompletion
    autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
    autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
    "autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
    autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
    autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
augroup END
" Enable heavy omni completion.
if !exists('g:neocomplcache_force_omni_patterns')
    let g:neocomplcache_force_omni_patterns = {}
endif
let g:neocomplcache_force_omni_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
let g:neocomplcache_force_omni_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplcache_force_omni_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

"dutyl configuration
"let g:dutyl_stdImportPaths=['/home/andrew/proj/ldc/runtime/druntime/src', '/home/andrew/proj/ldc/runtime/druntime/src/core', '/home/andrew/proj/ldc/runtime/phobos']
let g:dutyl_stdImportPaths = [ '/home/andrew/proj/ldc/runtime/druntime/src', '/home/andrew/proj/ldc/runtime/phobos' ]
let g:dutyl_neverAddClosingParen=1

if exists('*dutyl#register#tool')
    let s:dcd_client_path=exepath('dcd-client')
    if len(s:dcd_client_path) > 0
        call dutyl#register#tool('dcd-client', s:dcd_client_path)
    else
        call dutyl#register#tool('dcd-client', '/home/andrew/proj/DCD/bin/dcd-client')
    endif

    let s:dcd_server_path=exepath('dcd-server')
    if len(s:dcd_server_path) > 0
        call dutyl#register#tool('dcd-server', s:dcd_server_path)
    else
        call dutyl#register#tool('dcd-server', '/home/andrew/proj/DCD/bin/dcd-server')
    endif
endif

"incsearch settings
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

"easymotion settings
map <Leader> <Plug>(easymotion-prefix)

function! s:incsearch_config(...) abort
  return incsearch#util#deepextend(deepcopy({
  \   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
  \   'keymap': {
  \     "\<CR>": '<Over>(easymotion)'
  \   },
  \   'is_expr': 0
  \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Leader>/  incsearch#go(<SID>incsearch_config())
noremap <silent><expr> <Leader>?  incsearch#go(<SID>incsearch_config({'command': '?'}))
noremap <silent><expr> <Leader>g/ incsearch#go(<SID>incsearch_config({'is_stay': 1}))

function! s:config_easyfuzzymotion(...) abort
  return extend(copy({
  \   'converters': [incsearch#config#fuzzyword#converter()],
  \   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
  \   'keymap': {"\<CR>": '<Over>(easymotion)'},
  \   'is_expr': 0,
  \   'is_stay': 1
  \ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Space>/ incsearch#go(<SID>config_easyfuzzymotion())

"vim-submode
let g:submode_always_show_submode = 1

call submode#enter_with('window', 'n', '', '<C-w>')

call submode#leave_with('window', 'n', '', '<ESC>')

for s:key in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
\           'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    call submode#map('window', 'n', '', s:key, '<C-w>' . s:key)
    call submode#map('window', 'n', '', toupper(s:key), '<C-w>' . toupper(s:key))
    call submode#map('window', 'n', '', '<C-' . s:key. '>', '<C-w>' . '<C-' . s:key . '>')
endfor

for s:key in ['=', '_', '+', '-', '<', '>']
    call submode#map('window', 'n', '', s:key, '<C-w>' . s:key)
endfor

"ultisnips
let g:UltiSnipsExpandTrigger='<c-c>'
let g:UltiSnipsJumpForwardTrigger='<c-b>'
let g:UltiSnipsJumpBackwardTrigger='<c-z>'
let g:UltiSnipsEditSplit='context'

"rainbow_parentheses
augroup rainbow_parentheses
    autocmd VimEnter * RainbowParenthesesToggle
    autocmd Syntax * RainbowParenthesesLoadRound
    autocmd Syntax * RainbowParenthesesLoadSquare
    autocmd Syntax * RainbowParenthesesLoadBraces
augroup END

"rego
let g:formatdef_rego = '"opa fmt"'
let g:formatters_rego = ['rego']
let g:autoformat_autoindent = 1
let g:autoformat_retab = 1
au BufWritePre *.rego Autoformat
let g:autoformat_verbosemode = 1

"user defined commands
command! -complete=file -nargs=1 Te tabedit <args>
command! Cr color Revolution
command! Cg color gotham256
command! Cv color gruvbox | set background=dark
command! Cw StripWhitespace

"vim-go settings
let g:go_auto_type_info = 1
let g:go_addtags_transform = "snakecase"
let g:go_doc_keywordprg_enabled = 1
let g:go_highlight_build_constraits = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_metalinter_command = "golangci-lint"
let g:go_metalinter_autosave = 1
let g:go_metalinter_enabled = []
let g:go_metalinter_autosave_enabled = []
let g:go_jump_to_error = 0
let g:go_doc_url = 'https://godoc.comcast.com'
set updatetime=100

"title format
augroup general
    autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
    set title
augroup END

augroup aws
    function s:OpenS3File(file)
        let l:systemPath = substitute(a:file, 's3://', '/tmp/s3/', '')
        let l:cmd = 'aws s3 cp ' . shellescape(a:file) . ' ' . shellescape(l:systemPath)
        let l:output = system(l:cmd)
        echom(l:output)
        execute('0read ' . fnameescape(l:systemPath))
        redraw!
    endfunction

    function s:WriteS3File(file)
        let l:systemPath = substitute(a:file, "s3://", "/tmp/s3/", "")
        silent! execute 'write ' . fnameescape(l:systemPath)
        let l:cmd = "aws s3 cp " . shellescape(l:systemPath) . " " . shellescape(a:file)
        let l:output = system(l:cmd)
        redraw!
    endfunction

    autocmd!
    autocmd BufReadCmd s3://* call s:OpenS3File(expand("<amatch>"))
augroup END

"help files
augroup HelpInTabs
    autocmd!
    autocmd BufEnter *.txt call HelpInNewTab()
augroup END

augroup file_settings
    "makefile
    autocmd FileType * call Chtab(4)
    autocmd FileType make set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab

    "javascript
    autocmd FileType javascript call Chtab(2)

    "java
    autocmd FileType java setlocal omnifunc=javacomplete#Complete
    autocmd FileType java nmap <Leader>ja  <Plug>(JavaComplete-Imports-Add)
    autocmd FileType java nmap <Leader>jam <Plug>(JavaComplete-Imports-AddMissing)
    autocmd FileType java nmap <Leader>jru <Plug>(JavaComplete-Imports-RemoveUnused)

    "go
    autocmd FileType go nmap <leader>i :GoInfo<cr>
    autocmd FileType go nmap <leader>ga <Plug>(go-alternate-edit)
    autocmd FileType go nmap <leader>gah <Plug>(go-alternate-split)
    autocmd FileType go nmap <leader>gav <Plug>(go-alternate-vertical)
    autocmd FileType go nmap <F4> :GoCoverageToggle  -short<cr>
    autocmd FileType go nmap <F5> :GoTest -short<cr>

    "php
    autocmd FileType php call Chtab(4, 1)

    "Dlang
    "autocmd FileType d inoremap ( (<C-X><C-O>

    "Diet Template
    autocmd BufNewFile,BufReadPost *.dt set filetype=pug

    autocmd BufWinLeave * mkview
    autocmd BufWinEnter * silent! loadview

    autocmd BufRead * EditorConfigReload

    "protobuf
    autocmd FileType proto let g:ale_linters = { 'proto': [ 'api-linter' ] }
    "autocmd FileType proto let g:ale_linters = { 'proto': [ 'buf-lint', 'api-linter' ] }
    autocmd FileType proto let g:ale_lint_on_text_changed = 'never'
    autocmd FileType proto let g:ale_linters_explicit = 1
    autocmd FileType proto let g:ale_fixers = { 'proto': [ 'buf-format' ] }
    autocmd FileType proto let g:ale_fix_on_save = 1
augroup END

"spell settings
augroup spelling
    if has('spell')
        nn       <F6> :setlocal spell! spell?<CR>
        nn       <F7> :setlocal spell! spell?<CR>

        autocmd FileType tex set spell
        autocmd FileType gitcommit set spell
        autocmd FileType markdown set spell
    else
        nn       <F6> :echo     "I can't spell."
        nn       <F7> :echo     "I can't spell."
    endif
augroup END

if has('persistent_undo')
    set undofile

    set undodir=$HOME/.vim_undo_files

    set undolevels=5000
endif

"gpg settings (from mavam/dotfiles)
augroup encrypted
    autocmd!
    " First make sure nothing is written to ~/.viminfo while editing
    " an encrypted file.
    autocmd BufReadPre,FileReadPre      *.gpg set viminfo=
    " We don't want a swap file, as it writes unencrypted data to disk
    autocmd BufReadPre,FileReadPre      *.gpg set noswapfile
    " Switch to binary mode to read the encrypted file
    autocmd BufReadPre,FileReadPre      *.gpg set bin
    autocmd BufReadPre,FileReadPre      *.gpg let ch_save = &ch|set ch=2
    autocmd BufReadPost,FileReadPost    *.gpg '[,']!gpg --decrypt 2> /dev/null
    " Switch to normal mode for editing
    autocmd BufReadPost,FileReadPost    *.gpg set nobin
    autocmd BufReadPost,FileReadPost    *.gpg let &ch = ch_save|unlet ch_save
    autocmd BufReadPost,FileReadPost    *.gpg execute ":doautocmd BufReadPost " . expand("%:r")

    " Convert all text to encrypted text before writing
    autocmd BufWritePre,FileWritePre    *.gpg '[,']!gpg --default-recipient-self -ae 2>/dev/null
    " Undo the encryption so we are back in the normal text, directly
    " after the file has been written.
    autocmd BufWritePost,FileWritePost  *.gpg u
augroup END

augroup nerdtree
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERTTreeType") && b:NERDTreeType == "primary") | q | endif
augroup END

function! ReviewLastCommit()
    if exists('b:git_dir')
        Gtabedit HEAD^{}
        nnoremap <buffer> <silent> q :<C-U>bdelete<CR>
    else
        echo 'No git repository: ' expand('%:p')
    endif
endfunction

function! ShiftAndKeepVisualSelection(cmd)
    set nosmartindent
    if mode() =~ '[Vv]'
        return a:cmd . ":set smartindent\<CR>gv"
    else
        return a:cmd . ":set smartindent\<CR>"
    endif
endfunction

function! HelpInNewTab()
    if &buftype == 'help'
        execute "normal \<C-W>T"
    endif
endfunction

function! NumberToggle()
    if(&relativenumber == 1)
        set norelativenumber
    else
        set relativenumber
    endif
endfunction

nnoremap <C-N> :call NumberToggle()<cr>

"allow editing of RO files through root save
cmap w!! w !sudo tee % > /dev/null

"allow for per-project vimrc files and prohibit unsafe commands
set exrc
set secure
