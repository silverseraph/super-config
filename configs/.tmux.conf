# https://github.com/seebi/tmux-colors-solarized/blob/master/tmuxcolors-256.conf
set-option -g history-limit 8000

set-option -g status-bg default #base02
set-option -g status-fg colour136 #yellow
set-option -g status-style default

# set window split
bind-key v split-window -h
bind-key b split-window

# default window title colors
set-window-option -g window-status-style bg=default,fg=colour244

# active window title colors
set-window-option -g window-status-current-style bg=default,fg=colour166,bright

# pane border
set-option -g pane-border-style fg=colour235
set-option -g pane-active-border-style fg=colour240

# message text
set-option -g message-style bg=colour235,fg=colour166

# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange
# clock
set-window-option -g clock-mode-colour green #green

set -g status-interval 1
set -g status-justify centre # center align window list
set -g status-left-length 20
set -g status-right-length 140
set-option -g status on
set-option -g status-interval 2
set-option -g status-justify "centre"
set-option -g status-left-length 80
set-option -g status-right-length 90
set -g status-left '#[fg=green][#H]#[fg=black] #[fg=green,bright][#S]#[default] #[fg=green,bg=default,bright][cpu: #(tmux-top l)#[fg=green,bg=default,bright], mem: #(tmux-top m)#[fg=green,bg=default,bright]]'
set -g status-right '#[fg=green,bg=default][net: #(tmux-top n)#[fg=green,bg=default]] #[fg=green,bg=default][#(uptime | cut -f 4-5 -d " " | cut -f 1 -d ",")] #[fg=green,bg=default][%a%l:%M:%S %p] #[default]#[fg=green][%Y-%m-%d]'

# C-b is not acceptable -- Vim uses it
set-option -g prefix C-a
bind-key C-a last-window

# Start numbering at 1
set -g base-index 1

# Allows for faster key repetition
set -s escape-time 0

# Rather than constraining window size to the maximum size of any client
# connected to the *session*, constrain window size to the maximum size of any
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

# Allows us to use C-a a <command> to send commands to a TMUX session inside
# another TMUX session
bind-key a send-prefix

# Activity monitoring
setw -g monitor-activity on
set -g visual-activity on

# Vi copypaste mode
set-window-option -g mode-keys vi
bind-key -T copy-mode-vi 'v' send-keys -X begin-selection
bind-key -T copy-mode-vi 'y' send-keys -X copy-selection

# hjkl pane traversal
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# set to main-horizontal, 60% height for main pane
bind m set-window-option main-pane-height 60\; select-layout main-horizontal

bind-key C command-prompt -p "Name of new window: " "new-window -n '%%'"

# reload config
bind r source-file ~/.tmux.conf \; display-message "Config reloaded..."

# auto window rename
set-window-option -g window-status-current-format "#[fg=green,bg=default][#I #{pane_title} #W#F]#[default]"
set-window-option -g window-status-format "#[fg=green,dim,bg=default][#I #{pane_title} #W#F]#[default]"

# terminal name
set -g set-titles on
set -g set-titles-string "#T"
set-option -ga terminal-overrides ",xterm-256color:Tc"

# rm mouse mode fail
set -g mouse on
bind m run "if [ $(tmux show-options -g | grep mouse-select-pane | head -n 1 | cut \"-d 2\" -n 1) == \"on\" ]; then toggle=off; else toggle=on; fi; tmux display-message \"mouse: \$toggle\" &> /dev/null; for cmd in mode-mouse mouse-resize-pane mouse-select-pane mouse-select-window; do tmux set-option -g \$cmd \$toggle; done;"

set -g -q mouse on
bind m run "if [[ \"\$(tmux show-options -g | grep '^mouse ' | head -n 1 | cut '-d ' -f 2)\" == 'on' ]]; then toggle=off; else toggle=on; fi; tmux display-message \"mouse: \$toggle\"; tmux set-option -g -q mouse \$toggle;"
bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'select-pane -t=; copy-mode -e; send-keys -M'"
bind -n WheelDownPane select-pane -t= \; send-keys -M

set -g renumber-windows on

# color
set -g default-terminal "screen-256color"
#set -g default-terminal "tmux-256color"
set -ga terminal-overrides ",*256col*:Tc"

# environment
set -g update-environment -r

unbind c; bind c new-window -c "#{pane_current_path}"
unbind s; bind s split-window -v -c "#{pane_current_path}"
unbind v; bind v split-window -h -c "#{pane_current_path}"

set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @tpm_plugins 'tmux-plugins/tpm tmux-plugins/tmux-copycat'

run '~/.tmux/plugins/tpm/tpm'

bind b choose-session
