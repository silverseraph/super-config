local uricodec = {}

local char, gsub, tonumber, format, byte = string.char, string.gsub, tonumber, string.format, string.byte

local function tochar(hex)
    return char(tonumber(hex, 16))
end

uricodec.decode = function(uri)
    uri = gsub(uri, '%%(%x%x)', tochar)
    return uri
end

uricodec.encode = function(str)
    if str then
        str, _ = gsub(str, "\n", "\r\n")
        str, _ = gsub(str, "([^%w %-%_%.%~])",
                function(c) return format("%%%02X", byte(c)) end
            )
        str, _ = gsub(str, " ", "+")
    end
    return str
end

return uricodec
