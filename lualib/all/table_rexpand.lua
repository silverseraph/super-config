local tab = '    '

local function tprint(tcnt, k, v)
    print(tab:rep(tcnt) .. tostring(k) .. '::' .. tostring(v))
end

local function rexpand(table, depth, expanded_tables)
    if depth == nil then
        depth = 0
    end

    if expanded_tables == nil then
        expanded_tables = {}
    end

    for k,v in pairs(table) do
        if type(v) == type({}) then
            if expanded_tables[v] == nil then
                expanded_tables[v] = true
                tprint(depth, k, v)
                rexpand(v, depth + 1, expanded_tables)
            else
                tprint(depth, k, 'Index already expanded [' .. tostring(v) .. ']')
            end
        else
            tprint(depth, k, v)
        end
    end
end

return rexpand
